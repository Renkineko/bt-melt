"""
Gère les redirections du module bt
"""

from django.urls import path

from . import views

app_name = 'bt'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('manage/', views.BlindTestManager.as_view(), name='manage'),
    path('play/', views.BlindTestPlayer.as_view(), name='play'),
    
    path('participate/', views.BlindTestConnectFake.as_view(), name='participate'),
    path('disconnect/', views.BlindTestDisconnectFake.as_view(), name='disconnect'),
]
