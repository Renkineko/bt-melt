"""
Fichier requis par django_channels pour gérer la websocket.
"""
import json

from django.utils.text import slugify
from django.utils import timezone
from django.db.models import Sum

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from django.contrib.auth.models import User
from .models import BlindTest, Theme, Song, UserThemeAnswer, UserSongAnswer

class BlindTestConsumer(WebsocketConsumer):
    """
    Gestion de la communication bi-directionnelle entre les
    différents participants au blindtest.
    """
    bt_id = 'blindtest'
    bt_group_name = 'blindtest'
    
    def connect(self):
        self.bt_id = 'blindtest'
        self.bt_group_name = self.bt_id
        
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.bt_group_name,
            self.channel_name
        )
        
        if self.scope['user'].is_superuser:
            async_to_sync(self.channel_layer.group_send)(
                self.bt_group_name,
                {
                    'type': 'bt_gm_to_gm',
                    'params': {
                        "command": "recap",
                    }
                }
            )
        else:
            async_to_sync(self.channel_layer.group_send)(
                self.bt_group_name,
                {
                    'type': 'bt_gm_to_player',
                    'params': {
                        "command": "recap",
                        "target": self.scope['user'].username,
                    }
                }
            )
        
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.bt_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        """
        TODO
        """
        try:
            text_data_json = json.loads(text_data)
            command = text_data_json['command']
            
            # PARTIE GM BROADCAST (donc animateur -> players)
            
            if command == "start":
                if BlindTest.objects.count() == 0:
                    blindtest = BlindTest()
                else:
                    blindtest = BlindTest.objects.first()
                blindtest.active = True
                blindtest.save()
                
                blindtest.themes.all().delete()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "recap",
                        }
                    }
                )
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_gm',
                        'params': {
                            "command": "recap",
                        }
                    }
                )
            elif command == "stop":
                if BlindTest.objects.count() == 0:
                    blindtest = BlindTest()
                else:
                    blindtest = BlindTest.objects.first()
                blindtest.active = False
                blindtest.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "stop",
                        }
                    }
                )
            elif command == "no-theme":
                Theme.objects.all().update(active=False)
                
                theme = Theme()
                theme.blindtest = BlindTest.objects.filter(active=True).first()
                theme.hint = text_data_json['hint']
                theme.indice = text_data_json['number']
                theme.to_guess = False
                theme.locked = False
                theme.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "no-theme",
                            "number": text_data_json['number'],
                        }
                    }
                )
            elif command == "theme":
                Theme.objects.all().update(active=False)
                
                theme = Theme()
                theme.blindtest = BlindTest.objects.filter(active=True).first()
                theme.hint = text_data_json['hint']
                theme.indice = text_data_json['number']
                theme.to_guess = True
                theme.locked = False
                theme.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "theme",
                            "number": text_data_json['number'],
                        }
                    }
                )
            elif command == "song":
                Song.objects.all().update(active=False)
                
                song = Song()
                song.theme = Theme.objects.filter(active=True).last()
                song.hint = text_data_json['hint']
                song.indice = text_data_json['number']
                song.locked = False
                song.placeholder = text_data_json['placeholder']
                song.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "song",
                            "number": text_data_json['number'],
                            "placeholder": text_data_json['placeholder'],
                        }
                    }
                )
            elif command == "begin-timer-theme":
                theme = Theme.objects.last() # On ne time que le dernier, pas le choix.
                theme.started_timer = timezone.now()
                theme.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "begin-timer-theme",
                        }
                    }
                )
            elif command == "begin-timer-song":
                song = Song.objects.last() # On ne time que le dernier, pas le choix.
                song.started_timer = timezone.now()
                song.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_players',
                        'params': {
                            "command": "begin-timer-song",
                        }
                    }
                )
            
            # PARTIE JOUEUR A GM
            elif command == "theme-answer":
                theme = Theme.objects.filter(active=True).last()
                if theme.started_timer is not None:
                    delta = timezone.now() - theme.started_timer
                    if delta.total_seconds() > 15:
                        if not theme.locked:
                            theme.locked = True
                            theme.save()
                        # On ne répond pas après 15s de timer !
                        return
                if theme.locked or not theme.to_guess:
                    # On ne répond pas alors que le thème est verrouillé ou qu'il n'est
                    # pas devinable !
                    return
                    
                # Ok, on peut répondre. On vérifie s'il y a déjà eu une réponse pour ce thème
                # et ce joueur.
                if UserThemeAnswer.objects.filter(user=self.scope['user'], theme=theme).count() > 0:
                    uta = UserThemeAnswer.objects.get(user=self.scope['user'], theme=theme)
                    if uta.score is not None:
                        # On ne change pas sa réponse alors qu'elle a été notée !
                        return
                else:
                    uta = UserThemeAnswer()
                    uta.theme = theme
                    uta.user = self.scope['user']
                uta.answer = text_data_json['answer']
                uta.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_player_to_gm',
                        'params': {
                            "command": "theme-answer",
                            "username": self.scope['user'].username,
                            "answer": text_data_json['answer'],
                        }
                    }
                )
            elif command == "song-answer":
                song = Song.objects.filter(active=True).last()
                if song.started_timer is not None:
                    delta = timezone.now() - song.started_timer
                    if delta.total_seconds() > 15:
                        if not song.locked:
                            song.locked = True
                            song.save()
                        # On ne répond pas après 15s de timer !
                        return
                if song.locked:
                    # On ne répond pas alors que le morceau est verrouillé !
                    return
                    
                # Ok, on peut répondre. On vérifie s'il y a déjà eu une réponse pour ce morceau
                # et ce joueur.
                if UserSongAnswer.objects.filter(user=self.scope['user'], song=song).count() > 0:
                    usa = UserSongAnswer.objects.get(user=self.scope['user'], song=song)
                    if usa.score is not None:
                        # On ne change pas sa réponse alors qu'elle a été notée !
                        return
                else:
                    usa = UserSongAnswer()
                    usa.song = song
                    usa.user = self.scope['user']
                usa.answer = text_data_json['answer']
                usa.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_player_to_gm',
                        'params': {
                            "command": "song-answer",
                            "username": self.scope['user'].username,
                            "answer": text_data_json['answer'],
                        }
                    }
                )
            elif command == "cancel-theme-answer":
                theme = Theme.objects.filter(active=True).last()
                if theme.started_timer is not None:
                    delta = timezone.now() - theme.started_timer
                    if delta.total_seconds() > 15:
                        if not theme.locked:
                            theme.locked = True
                            theme.save()
                        # On ne répond pas après 15s de timer !
                        return
                if theme.locked or not theme.to_guess:
                    # On ne répond pas alors que le thème est verrouillé ou qu'il n'est
                    # pas devinable !
                    return
                    
                # Ok, on peut répondre. On vérifie s'il y a déjà eu une réponse pour ce thème
                # et ce joueur.
                if UserThemeAnswer.objects.filter(user=self.scope['user'], theme=theme).count() > 0:
                    uta = UserThemeAnswer.objects.get(user=self.scope['user'], theme=theme)
                    if uta.score is not None:
                        # On ne change pas sa réponse alors qu'elle a été notée !
                        return
                else:
                    uta = UserThemeAnswer()
                    uta.theme = theme
                    uta.user = self.scope['user']
                uta.answer = ''
                uta.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_player_to_gm',
                        'params': {
                            "command": "cancel-theme-answer",
                            "username": self.scope['user'].username,
                        }
                    }
                )
            elif command == "cancel-song-answer":
                song = Song.objects.filter(active=True).last()
                if song.started_timer is not None:
                    delta = timezone.now() - song.started_timer
                    if delta.total_seconds() > 15:
                        if not song.locked:
                            song.locked = True
                            song.save()
                        # On ne répond pas après 15s de timer !
                        return
                if song.locked:
                    # On ne répond pas alors que le morceau est verrouillé !
                    return
                    
                # Ok, on peut répondre. On vérifie s'il y a déjà eu une réponse pour ce morceau
                # et ce joueur.
                if UserSongAnswer.objects.filter(user=self.scope['user'], song=song).count() > 0:
                    usa = UserSongAnswer.objects.get(user=self.scope['user'], song=song)
                    if usa.score is not None:
                        # On ne change pas sa réponse alors qu'elle a été notée !
                        return
                else:
                    usa = UserSongAnswer()
                    usa.song = song
                    usa.user = self.scope['user']
                usa.answer = ''
                usa.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_player_to_gm',
                        'params': {
                            "command": "cancel-song-answer",
                            "username": self.scope['user'].username,
                        }
                    }
                )
            elif command == "get-final-results":
                blindtest = BlindTest.objects.prefetch_related('themes', 'themes__songs').first()
                
                if blindtest.active:
                    raise Exception("Le blind-test est toujours en cours, attendez la fin pour avoir les résultats.", "final_results_unavailable_while_bt_running")
                
                users_json = []
                
                users = User.objects.filter(
                    is_superuser=False
                ).annotate(total_song=Sum('usersonganswer__score')).all()
                
                for usr in users:
                    total_theme = UserThemeAnswer.objects.filter(
                        user=usr
                    ).aggregate(Sum('score'))['score__sum']
                    
                    if total_theme is None:
                        total_theme = 0
                    
                    total_song = usr.total_song
                    
                    if total_song is None:
                        total_song = 0
                    
                    if total_theme + total_song > 0:
                        users_json.append({
                            "username": usr.username,
                            "points": total_theme + total_song,
                        })
                    
                sorted_users_json = sorted(users_json, key = lambda i: (-i['points'], i['username']))
                
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_player',
                        'params': {
                            "command": "get-final-results",
                            "target": self.scope['user'].username,
                            "results": sorted_users_json,
                        }
                    }
                )
                
                
            
            # PARTIE GM A JOUEUR
            elif command == "note-theme":
                relid = text_data_json['relid']
                indice = relid[1:]
                theme = Theme.objects.filter(indice=indice).last()
                uta = UserThemeAnswer.objects.filter(
                    theme=theme,
                    user__username=text_data_json['username']
                ).first()
                
                uta.answer = text_data_json['answer']
                uta.score = text_data_json['score']
                uta.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_player',
                        'params': {
                            "command": "note-theme",
                            "score": text_data_json['score'],
                            "answer": text_data_json['answer'],
                            "theme": text_data_json['relid'],
                            "target": text_data_json['username'],
                        }
                    }
                )
            elif command == "cancel-note-theme":
                relid = text_data_json['relid']
                indice = relid[1:]
                theme = Theme.objects.filter(indice=indice).last()
                uta = UserThemeAnswer.objects.filter(
                    theme=theme,
                    user__username=text_data_json['username']
                ).first()
                
                uta.score = None
                uta.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_player',
                        'params': {
                            "command": "cancel-note-theme",
                            "theme": text_data_json['relid'],
                            "target": text_data_json['username'],
                        }
                    }
                )
            elif command == "note-song":
                relid = text_data_json['relid']
                theme_indice = relid[1:relid.index('s')]
                song_indice = relid[relid.index('s')+1:]
                song = Song.objects.filter(theme__indice=theme_indice, indice=song_indice).last()
                usa = UserSongAnswer.objects.filter(
                    song=song,
                    user__username=text_data_json['username']
                ).first()
                
                usa.answer = text_data_json['answer']
                usa.score = text_data_json['score']
                usa.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_player',
                        'params': {
                            "command": "note-song",
                            "score": text_data_json['score'],
                            "answer": text_data_json['answer'],
                            "song": text_data_json['relid'],
                            "target": text_data_json['username'],
                        }
                    }
                )
            elif command == "cancel-note-song":
                relid = text_data_json['relid']
                theme_indice = relid[1:relid.index('s')]
                song_indice = relid[relid.index('s')+1:]
                song = Song.objects.filter(theme__indice=theme_indice, indice=song_indice).last()
                usa = UserSongAnswer.objects.filter(
                    song=song,
                    user__username=text_data_json['username']
                ).first()
                
                usa.score = None
                usa.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.bt_group_name,
                    {
                        'type': 'bt_gm_to_player',
                        'params': {
                            "command": "cancel-note-song",
                            "song": text_data_json['relid'],
                            "target": text_data_json['username'],
                        }
                    }
                )
            
            else:
                raise Exception('Je ne vois pas ce que je dois faire.', 'unknown_command')
            
            
            
        except Exception as raised_except:
            try:
                # import pdb; pdb.set_trace()
                self.send(text_data=json.dumps({
                    'error': {
                        "message": raised_except.args[0],
                        "code": raised_except.args[1],
                    }
                }))
            except Exception as raised_except:
                self.send(text_data=json.dumps({
                    'error': {
                        "message": str(raised_except),
                        "code": "",
                    }
                }))
        
    # Receive message from room group
    def bt_gm_to_players(self, event):
        """
        Envoie des informations de l'animateur aux joueurs
        """
        params = event['params']
        
        if self.scope['user'].is_superuser:
            return
            
        if params['command'] == 'recap':
            blindtest = BlindTest.objects.prefetch_related('themes', 'themes__songs').first()
            
            blindtest_json = {
                "active": blindtest.active,
                "themes": [],
            }
            
            params["bt"] = blindtest_json
        
        self.send(text_data=json.dumps(params))
        
    # Receive message from room group
    def bt_gm_to_player(self, event):
        """
        Envoie des informations de l'animateur à un joueur
        """
        params = event['params']
        
        if params['target'] != self.scope['user'].username:
            return
        
        if params['command'] == 'recap':
            blindtest = BlindTest.objects.prefetch_related('themes', 'themes__songs').first()
            
            blindtest_json = {
                "active": blindtest.active,
                "themes": [],
            }
            
            for theme in blindtest.themes.all():
                theme_json = {
                    "number": theme.indice,
                    "to_guess": theme.to_guess,
                    "locked": theme.locked,
                    "active": theme.active,
                    "seconds_left": None,
                    "answer": "",
                    "score": None,
                    "songs": [],
                }
                
                if theme.to_guess:
                    uta = UserThemeAnswer.objects.filter(
                        user=self.scope['user'],
                        theme=theme,
                    )
                    if len(uta) > 0:
                        uta = uta.first()
                        theme_json["answer"] = uta.answer
                        theme_json["score"] = uta.score
                    if not theme.locked and theme.started_timer is not None:
                        delta = timezone.now() - theme.started_timer
                        if delta.total_seconds() > 15:
                            theme.locked = True
                            theme.save()
                            theme_json["locked"] = True
                            theme_json["seconds_left"] = 0
                        else:
                            theme_json["seconds_left"] = int(15 - delta.total_seconds())
                    
                for song in theme.songs.all():
                    song_json = {
                        "number": song.indice,
                        "locked": song.locked,
                        "active": song.active,
                        "placeholder": song.placeholder,
                        "seconds_left": None,
                        "answer": "",
                        "score": None,
                    }
                    
                    usa = UserSongAnswer.objects.filter(
                        user=self.scope['user'],
                        song=song,
                    )
                    
                    if len(usa) > 0:
                        usa = usa.first()
                        song_json["answer"] = usa.answer
                        song_json["score"] = usa.score
                    if not song.locked and song.started_timer is not None:
                        delta = timezone.now() - song.started_timer
                        if delta.total_seconds() > 15:
                            song.locked = True
                            song.save()
                            song_json["locked"] = True
                            song_json["seconds_left"] = 0
                        else:
                            song_json["seconds_left"] = int(15 - delta.total_seconds())
                    
                    theme_json['songs'].append(song_json)
                    
                blindtest_json['themes'].append(theme_json)
            
            params["bt"] = blindtest_json
        
        total_theme = UserThemeAnswer.objects.filter(
            user=self.scope['user']
        ).aggregate(Sum('score'))['score__sum']
        
        if total_theme is None:
            total_theme = 0
        
        total_song = UserSongAnswer.objects.filter(
            user=self.scope['user']
        ).aggregate(Sum('score'))['score__sum']
        
        if total_song is None:
            total_song = 0
        
        params['total_score'] = total_theme + total_song
        
        self.send(text_data=json.dumps(params))
        
    # Receive message from room group
    def bt_player_to_gm(self, event):
        """
        Envoie des informations d'un joueur à l'animateur
        """
        params = event['params']
        
        if not self.scope['user'].is_superuser:
            return
        
        params['slugified_username'] = slugify(params['username'])
        self.send(text_data=json.dumps(params))
        
    # Receive message from room group
    def bt_gm_to_gm(self, event):
        """
        Envoie des informations à l'animateur (souvent le récap)
        """
        params = event['params']
        
        if not self.scope['user'].is_superuser:
            return
        
        if BlindTest.objects.count() > 0:
            blindtest = BlindTest.objects.prefetch_related('themes', 'themes__songs').first()
        else:
            blindtest = BlindTest()
            blindtest.save()
        
        blindtest = BlindTest.objects.prefetch_related('themes', 'themes__songs').first()
        
        blindtest_json = {
            "active": blindtest.active,
            "themes": [],
        }
        
        for theme in blindtest.themes.all():
            theme_json = {
                "number": theme.indice,
                "hint": theme.hint,
                "active": theme.active,
                "locked": theme.locked,
                "to_guess": theme.to_guess,
                "seconds_left": None,
                "answers": [],
                "songs": [],
            }
            
            if theme.to_guess:
                for uta in UserThemeAnswer.objects.filter(theme=theme).all():
                    uta_json = {
                        "answer": uta.answer,
                        "score": uta.score,
                        "username": uta.user.username,
                        "slugified_username": slugify(uta.user.username),
                    }
                    
                    theme_json["answers"].append(uta_json)
                
                if not theme.locked and theme.started_timer is not None:
                    delta = timezone.now() - theme.started_timer
                    if delta.total_seconds() > 15:
                        theme.locked = True
                        theme.save()
                        theme_json["locked"] = True
                        theme_json["seconds_left"] = 0
                    else:
                        theme_json["seconds_left"] = int(15 - delta.total_seconds())
                
            for song in theme.songs.all():
                song_json = {
                    "number": song.indice,
                    "hint": song.hint,
                    "active": song.active,
                    "locked": song.locked,
                    "seconds_left": None,
                    "answers": [],
                }
                
                for usa in UserSongAnswer.objects.filter(song=song).all():
                    usa_json = {
                        "answer": usa.answer,
                        "score": usa.score,
                        "username": usa.user.username,
                        "slugified_username": slugify(usa.user.username),
                    }
                    
                    song_json['answers'].append(usa_json)
                
                if not song.locked and song.started_timer is not None:
                    delta = timezone.now() - song.started_timer
                    if delta.total_seconds() > 15:
                        song.locked = True
                        song.save()
                        song_json["locked"] = True
                        song_json["seconds_left"] = 0
                    else:
                        song_json["seconds_left"] = int(15 - delta.total_seconds())
                
                theme_json['songs'].append(song_json)
                
            blindtest_json['themes'].append(theme_json)
        
        params['bt'] = blindtest_json
        
        self.send(text_data=json.dumps(params))
