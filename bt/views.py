"""
Gère les vues pour le blind test
"""
# from random import choice

from django.views import generic
from django.shortcuts import render, redirect

from django.contrib.auth import login, logout

from django.contrib.auth.models import User

# Create your views here.
class IndexView(generic.View):
    """
    Page d'accueil sur laquelle on se connecte
    """
    
    def get(self, request):
        """
        Lorsqu'on arrive sur la page via le get (donc pas en formulaire),
        si on est déjà connecté, on supprime l'utilisateur.
        """
        # if request.user.is_authenticated and not request.user.is_superuser:
        #     # TODO Attention : vrai uniquement pour les comptes "anonymes" mais
        #     # pas pour les comptes twitch ! A voir comment on fait !
        #     # import pdb; pdb.set_trace()
        #     user = User.objects.filter(username=request.user.username)
        #     logout(request)
        #     user.delete()
            
        return render(request, 'bt/index.html')

class BlindTestManager(generic.View):
    """
    Gère le blind test lui-même, côté animateur
    """
    
    def get(self, request):
        """
        Si on est pas un superuser, on est redirigé sur play (petit canaillou)
        """
        if not request.user.is_authenticated or not request.user.is_superuser:
            return redirect('bt:play')
            
        return render(request, 'bt/manage.html')
        
class BlindTestConnectFake(generic.View):
    """
    Gère la création d'un compte avant d'être redirigé sur play
    """
    
    def post(self, request):
        """
        On arrive depuis un formulaire. Si l'username existe déjà, on essaye
        de rajouter un suffixe. Pour la beauté de l'humour.
        """
        
        if request.user.is_authenticated:
            # L'utilisateur est DÉJÀ authentifié, on le renvoie du coup sur le play sans s'emmerder plus que ça.
            return redirect('bt:play')
        
        # suffixes = ['à la butte arc-en-ciel', 'au nounours bien léché',
        #             'à la joie indémontable', "à l'amour inconsidéré",
        #             'au destin fabuleux', 'au recyclage infini',
        #             'à la belle escapade', "à l'oreille absolue",
        #             "à l'originalité folle", 'aux attributs exacerbés',
        #             'aux amitiés multiples', "à l'équidé magique",
        #             'à la célérité incroyable', 'à la précision redoutable',
        #             'à la connaissance exceptionnelle', 'à la fermeté exemplaire',
        #             'aux oreilles pointues', 'à la bouche pulpeuse',
        #             'aux yeux miroitants', "à l'espoir éperdu",
        #             'à la félicité étoilée', 'à la fleur goguenarde',
        #             'au mécréant occis', 'au roseau pliable',
        #             'à la jonquille olfactive', 'au nénufar étendu',
        #             'aux réflexes surhumains', 'à la miséricorde complète',
        #             'au pardon inatteignable', 'au rangement intéressant',
        #             'à la peau veloutée', 'à la chevelure soyeuse',
        #             'au sourire étincelant', "à l'hygiène irréprochable"
        #             'au guet gai', 'au clan déclinant', 'à la taille extraordinaire']
        
        posted_username = request.POST['nickname']
        if posted_username == '':
            posted_username = 'Anonyme'
        
        username = posted_username
        
        inc = 1
        while len(User.objects.filter(username=username)) > 0:
            # username = "%s %s" % (posted_username, choice(suffixes))
            username = "%s %d" % (posted_username, inc)
            inc = inc + 1
        
        user = User.objects.create_user(username=username)
        
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        
        return redirect('bt:play')

class BlindTestDisconnectFake(generic.View):
    """
    Gère la suppression d'un compte avant d'être redirigé sur l'accueil
    """
    
    def get(self, request):
        """
        Si un compte est connecté, on le déconnecte et on le supprime. Aussi simple que ça.
        """
        if request.user.is_authenticated and not request.user.is_superuser:
            # TODO Attention : vrai uniquement pour les comptes "anonymes" mais
            # pas pour les comptes twitch ! A voir comment on fait !
            # import pdb; pdb.set_trace()
            user = User.objects.filter(username=request.user.username)
            logout(request)
            user.delete()
        
        return redirect('bt:index')

class BlindTestPlayer(generic.View):
    """
    Affichage du formulaire de jeu côté joueur
    """
    
    def get(self, request):
        """
        Si on est pas connecté, on est redirigé sur l'index, sinon on peut jouer :D
        """
        if not request.user.is_authenticated:
            return redirect('bt:index')
        return render(request, 'bt/play.html')
