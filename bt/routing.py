from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r'^wbsckt/bt/$', consumers.BlindTestConsumer.as_asgi()),
]
