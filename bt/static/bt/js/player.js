// polyfill prepend
// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/prepend()/prepend().md
(function (arr) {
	arr.forEach(function (item) {
	  if (item.hasOwnProperty('prepend')) {
		return;
	  }
	  Object.defineProperty(item, 'prepend', {
		configurable: true,
		enumerable: true,
		writable: true,
		value: function prepend() {
		  var argArr = Array.prototype.slice.call(arguments),
			docFrag = document.createDocumentFragment();
		  
		  argArr.forEach(function (argItem) {
			var isNode = argItem instanceof Node;
			docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
		  });
		  
		  this.insertBefore(docFrag, this.firstChild);
		}
	  });
	});
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]);

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/append()/append().md
(function (arr) {
	arr.forEach(function (item) {
	  if (item.hasOwnProperty('append')) {
		return;
	  }
	  Object.defineProperty(item, 'append', {
		configurable: true,
		enumerable: true,
		writable: true,
		value: function append() {
		  var argArr = Array.prototype.slice.call(arguments),
			docFrag = document.createDocumentFragment();
  
		  argArr.forEach(function (argItem) {
			var isNode = argItem instanceof Node;
			docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
		  });
  
		  this.appendChild(docFrag);
		}
	  });
	});
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]);

$(function() {
	// Initialisation websocket
	var websocket;
	var theme_id;
	var song_id;
	
	function bt_start() {
		$('#wait').addClass('d-none');
		$('#play').removeClass('d-none');
		$('#totalScore').text('-');
		
		$('#themeAnswer').addClass('d-none');
		$('#lastThemeAnswer').text('Aucune...');
		$('#cancelLastThemeAnswer').addClass('d-none');
		$('#cancelLastThemeAnswer button').prop('disabled', true);
		$('#scoreLastThemeAnswer').addClass('d-none');
		$('#lockedTheme').addClass('d-none');
		$('#waitingCorrectionTheme').addClass('d-none');
		$('#correctedTheme').removeClass('d-none');
		
		$('#songAnswer').addClass('d-none');
		$('#lastSongAnswer').text('Aucune...');
		$('#cancelLastSongAnswer').addClass('d-none');
		$('#cancelLastSongAnswer button').prop('disabled', true);
		$('#scoreLastSongAnswer').addClass('d-none');
		$('#lockedSong').addClass('d-none');
		$('#waitingCorrectionSong').addClass('d-none');
		$('#correctedSong').removeClass('d-none');
		
		$('#btnFinalResults').addClass('d-none');
	}
	
	function bt_stop() {
		$('#play').addClass('d-none');
		$('#wait').removeClass('d-none');
		
		$('#themeAnswer').addClass('d-none');
		$('#lastThemeAnswer').text('Aucune...');
		$('#cancelLastThemeAnswer').addClass('d-none');
		$('#cancelLastThemeAnswer button').prop('disabled', true);
		$('#scoreLastThemeAnswer').addClass('d-none');
		$('#lockedTheme').addClass('d-none');
		$('#waitingCorrectionTheme').addClass('d-none');
		$('#correctedTheme').removeClass('d-none');
		
		$('#songAnswer').addClass('d-none');
		$('#lastSongAnswer').text('Aucune...');
		$('#cancelLastSongAnswer').addClass('d-none');
		$('#cancelLastSongAnswer button').prop('disabled', true);
		$('#scoreLastSongAnswer').addClass('d-none');
		$('#btnFinalResults').addClass('d-none');
		$('#waitingCorrectionSong').addClass('d-none');
		$('#correctedSong').removeClass('d-none');
		
		$('#btnFinalResults').removeClass('d-none');
	}
	
	function new_theme(number, to_guess, only_history, score, answer) {
		theme_id = 't' + number;
		
		var suffixe = '';
		if (to_guess) {
			var txt_answer = '<i class="fas fa-ban"></i>';
			var txt_score = 'NC';
			if (answer && answer !== '') {
				txt_answer = answer;
			}
			if (score) {
				txt_score = score;
			}
			suffixe = ' - Réponse : <span class="answer">' + txt_answer + '</span> (Points : <span class="points">' + txt_score + '</span>)';
		}
		
		$('#themes').prepend('<li id="li-' + theme_id + '" class="theme guessed"> Thème N°' + number + suffixe +'<ul id="ul-' + theme_id + '"></ul></li>');
		
		if (only_history) {
			return;
		}
		
		if (to_guess) {
			$('#themeAnswer').removeClass('d-none');
		}
		else {
			$('#themeAnswer').addClass('d-none');
		}
		$('#themeAnswer input[type=text]').val('').prop('disabled', false);
		$('#themeAnswer input[type=submit]').prop('disabled', false);
		$('#songAnswer').addClass('d-none');
		$('#lastThemeAnswer').text('Aucune...');
		$('#cancelLastThemeAnswer').addClass('d-none');
		$('#cancelLastThemeAnswer button').prop('disabled', true);
		$('#scoreLastThemeAnswer').addClass('d-none');
		$('#lockedTheme').addClass('d-none');
		$('#waitingCorrectionTheme').addClass('d-none');
		$('#correctedTheme').removeClass('d-none');
	}
	
	function new_song(number, placeholder, only_history, score, answer) {
		song_id = theme_id + 's' + number;
		
		var txt_answer = '<i class="fas fa-ban"></i>';
		var txt_score = 'NC';
		if (answer && answer !== '') {
			txt_answer = answer;
		}
		if (score) {
			txt_score = score;
		}
		var suffixe = ' - Réponse : <span class="answer">' + txt_answer + '</span> (Points : <span class="points">' + txt_score + '</span>)';
		
		$('#ul-'+theme_id).prepend('<li id="li-' + song_id + '" class="song"> Musique N°' + number + suffixe + '</li>');
		
		if (only_history) {
			return;
		}
		
		$('#songAnswer').removeClass('d-none');
		$('#songAnswer input[type=text]').val('').prop('disabled', false).attr('placeholder', placeholder);
		$('#songAnswer input[type=submit]').prop('disabled', false);
		$('#lastSongAnswer').text('Aucune...');
		$('#cancelLastSongAnswer').addClass('d-none');
		$('#cancelLastSongAnswer button').prop('disabled', true);
		$('#scoreLastSongAnswer').addClass('d-none');
		$('#lockedSong').addClass('d-none');
		$('#waitingCorrectionSong').addClass('d-none');
		$('#correctedSong').removeClass('d-none');
	}
	
	function lock_answer(target, score, answer, target_id) {
		var li = $('#li-' + target_id);
		var only_history = false;
		switch (target) {
			case 'Theme':
				only_history = target_id !== theme_id;
			break;
			
			case 'Song':
				only_history = target_id !== song_id;
			break;
			
			default:
				return;
		}
		
		if (score === null) {
			score = 'NC';
		}
		
		if (!only_history) {
			$('#cancelLast' + target + 'Answer').addClass('d-none');
			$('#scoreLast' + target + 'Answer').removeClass('d-none');
			$('#scoreLast' + target + 'Answer .score').text(score);
			$('#last' + target + 'Answer').text(answer);
			$('#input' + target + 'Answer').prop('disabled', true).val('');
			
			if (score === 'NC') {
				$('#corrected' + target).addClass('d-none');
				$('#waitingCorrection' + target).removeClass('d-none');
			}
			else {
				$('#corrected' + target).removeClass('d-none');
				$('#waitingCorrection' + target).addClass('d-none');
			}
		}
		
		if (answer === '') {
			answer = '<i class="fas fa-ban"></i>';
		}
		
		li.find('span.answer:first').html(answer);
		li.find('span.points:first').text(score);
	}
	
	function unlock_answer(target, target_id) {
		var li = $('#li-' + target_id);
		var only_history = false;
		switch (target) {
			case 'Theme':
				only_history = target_id !== theme_id;
			break;
			
			case 'Song':
				only_history = target_id !== song_id;
			break;
			
			default:
				return;
		}
		
		if (!only_history) {
			$('#cancelLast' + target + 'Answer').removeClass('d-none');
			$('#scoreLast' + target + 'Answer').addClass('d-none');
			$('#scoreLast' + target + 'Answer .score').text('');
			$('#input' + target + 'Answer').prop('disabled', false);
			
			$('#corrected' + target).addClass('d-none');
			$('#waitingCorrection' + target).removeClass('d-none');
		}
		
		li.find('span.points:first').text('NC');
	}
	
	function submit_answer(target, answer) {
		$('#input' + target + 'Answer').val('');
		$('#last' + target + 'Answer').text(answer);
		$('#cancelLast' + target + 'Answer').removeClass('d-none');
		$('#cancelLast' + target + 'Answer button').prop('disabled', false);
		$('#scoreLast' + target + 'Answer').addClass('d-none');
		
		var li;
		switch (target) {
			case 'Theme':
				li = $('#li-' + theme_id);
			break;
			
			case 'Song':
				li = $('#li-' + song_id);
			break;
			
			default:
				return;
		}
		
		li.find('span.answer:first').text(answer);
		li.find('span.points:first').text('NC');
	}
	
	function cancel_answer(target) {
		$('#last' + target + 'Answer').text('Aucune...');
		$('#cancelLast' + target + 'Answer').addClass('d-none');
		$('#cancelLast' + target + 'Answer button').prop('disabled', true);
		$('#scoreLast' + target + 'Answer').addClass('d-none');
		$('#corrected' + target).addClass('d-none');
		$('#waitingCorrection' + target).removeClass('d-none');
		
		var li;
		switch (target) {
			case 'Theme':
				li = $('#li-' + theme_id);
			break;
			
			case 'Song':
				li = $('#li-' + song_id);
			break;
			
			default:
				return;
		}
		
		li.find('span.answer:first').html('<i class="fas fa-ban"></i>');
		li.find('span.points:first').html('NC');
	}
	
	function decompte(target, timer) {
		if (timer <= 0) {
			$('#' + target + ' input[type=text]').prop('disabled', true).val('');
			$('#' + target + ' input[type=submit]').prop('disabled', true);
			$('#' + target + ' span.cancel-answer').addClass('d-none');
			$('#' + target + ' span.cancel-answer button').prop('disabled', true);
			$('#' + target + ' h4').addClass('d-none');
			
			$('#' + target + ' .fa-lock').parent().removeClass('d-none');
		}
		else {
			$('#' + target + ' .timer').text(timer);
			$('#' + target + ' h4').removeClass('d-none');
			timer--;
			setTimeout(function() {
				decompte(target, timer);
			}, 1000);
		}
		
	}
	
	function ws_init() {
		var protocole = location.protocol.split(':')[0] === 'https' ? 'wss' : 'ws';
		websocket = new WebSocket(protocole + '://'+location.host+'/wbsckt/bt/');
		
		websocket.onmessage = function(e) {
			var data = JSON.parse(e.data);
			
			// console.log(data);
			
			if (data.error) {
				console.error(data.error);
				return;
			}
			
			switch(data['command']) {
				
				case 'recap':
					if (data['bt']) {
						if (data['bt'].active) {
							bt_start();
						}
						else {
							bt_stop();
						}
						
						var active_theme = null;
						var active_song = null;
						$('#themes').empty();
						
						data['bt'].themes.forEach(function(theme) {
							if (theme.active) {
								active_theme = theme;
							}
							new_theme(theme.number, theme.to_guess, !theme.active, theme.score, theme.answer);
							
							theme.songs.forEach(function(song) { 
								if (song.active) {
									active_song = song;
								}
								new_song(song.number, song.placeholder, !song.active, song.score, song.answer);
							});
						});
						
						if (active_theme) {
							// new_theme(last_theme.number, last_theme.to_guess);
							
							if (active_theme.to_guess) {
								if (!active_theme.locked) {
									$('#lockedTheme').addClass('d-none');
									if (active_theme.seconds_left > 0) {
										decompte('themeAnswer', active_theme.seconds_left);
									}
									
									if (active_theme.score !== null && active_theme.score >= 0) {
										lock_answer('Theme', active_theme.score, active_theme.answer, theme_id);
									}
									else {
										unlock_answer('Theme', theme_id);
										if (active_theme.answer !== '') {
											submit_answer('Theme', active_theme.answer);
										}
										else {
											cancel_answer('Theme');
										}
									}
								}
								else {
									lock_answer('Theme', active_theme.score, active_theme.answer, theme_id);
									$('#lockedTheme').removeClass('d-none');
								}
							}
							
							if (active_song) {
								// new_song(last_song.number);
								
								if (!active_song.locked) {
									$('#lockedSong').addClass('d-none');
									if (active_song.seconds_left > 0) {
										decompte('songAnswer', active_song.seconds_left);
									}
									
									if (active_song.score !== null && active_song.score >= 0) {
										lock_answer('Song', active_song.score, active_song.answer, song_id);
									}
									else {
										unlock_answer('Song', song_id);
										if (active_song.answer !== '') {
											submit_answer('Song', active_song.answer);
										}
										else {
											cancel_answer('Song');
										}
									}
								}
								else {
									lock_answer('Song', active_song.score, active_song.answer, song_id);
									$('#lockedSong').removeClass('d-none');
								}
							}
						}
					}
					
				break;
				
				
				case 'start':
					bt_start();
				break;
				
				case 'stop':
					bt_stop();
				break;
				
				case 'theme':
					new_theme(data['number'], true);
				break;
				
				case 'no-theme':
					new_theme(data['number'], false);
				break;
				
				
				case 'song':
					new_song(data['number'], data['placeholder']);
				break;
				
				case 'note-theme':
					lock_answer('Theme', data['score'], data['answer'], data['theme']);
				break;
				
				case 'note-song':
					lock_answer('Song', data['score'], data['answer'], data['song']);
				break;
				
				case 'cancel-note-theme':
					unlock_answer('Theme', data['theme']);
				break;
				
				case 'cancel-note-song':
					unlock_answer('Song', data['song']);
				break;
				
				case 'begin-timer-theme':
					decompte('themeAnswer', 15);
				break;
				
				case 'begin-timer-song':
					decompte('songAnswer', 15);
				break;
				
				case 'get-final-results':
					$('#finalResultsDiv').empty();
					data['results'].forEach(function(score) {
						var node = document.importNode(document.querySelector('#tplResult').content, true);
						node.querySelector('span.finalResultUsername').textContent = score.username;
						node.querySelector('span.finalResultPoints').textContent = score.points !== null ? score.points : 'NC';
						document.querySelector('#finalResultsDiv').append(node);
					});
					
					$('#finalResults').modal('show');
				break;
			}
			
			if (data['total_score'] !== null) {
				$('#totalScore').text(data['total_score']);
			}
		};
	}
	
	function ws_send(message) {
		switch (websocket.readyState) {
			case websocket.CONNECTING:
				return setTimeout(function() {
					ws_send(message)
				}, 200);
				
			case websocket.OPEN:
				return websocket.send(JSON.stringify(message));
				
			case websocket.CLOSED:
			case websocket.CLOSING:
				ws_init();
				return setTimeout(function() {
					ws_send(message)
				}, 200);
		}
	}
	
	ws_init();
	
	// Initialisation buttons et forms
	$('#expectedThemeAnswerForm').off('submit').on('submit', function() {
		var answer = $('#inputThemeAnswer').val();
		
		if (answer === '') {
			// Eviter les erreurs genre double entrée ou autre qui annule de fait la réponse. Si on veut l'annuler on clique sur annuler puis c'est marre.
			return false;
		}
		
		ws_send({
			"command": "theme-answer",
			"answer": answer,
		});
		
		submit_answer('Theme', answer);
		
		return false;
	});
	
	$('#expectedSongAnswerForm').off('submit').on('submit', function() {
		var answer = $('#inputSongAnswer').val();
		
		if (answer === '') {
			// Eviter les erreurs genre double entrée ou autre qui annule de fait la réponse. Si on veut l'annuler on clique sur annuler puis c'est marre.
			return false;
		}
		
		ws_send({
			"command": "song-answer",
			"answer": answer,
		});
		
		submit_answer('Song', answer);
		
		return false;
	});
	
	$('#cancelLastThemeAnswer button').off('click').on('click', function() {
		ws_send({
			"command": "cancel-theme-answer",
		});
		
		cancel_answer('Theme');
	});
	$('#cancelLastSongAnswer button').off('click').on('click', function() {
		ws_send({
			"command": "cancel-song-answer",
		});
		
		cancel_answer('Song');
	});
	$('#btnFinalResults').off('click').on('click', function() {
		ws_send({
			"command": "get-final-results",
		});
	});
	
	// Sleep/wake up management for mobile device
	
	// Set the name of the hidden property and the change event for visibility
	var hidden, visibilityChange; 
	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
		hidden = "hidden";
		visibilityChange = "visibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
		hidden = "msHidden";
		visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
		hidden = "webkitHidden";
		visibilityChange = "webkitvisibilitychange";
	}
	
	// When the page is shown, ask the websocket to reinit itself.
	function handleVisibilityChange() {
		if (!document[hidden]) {
			websocket.close();
			ws_init();
		}
	}

	// Warn if the browser doesn't support addEventListener or the Page Visibility API
	if (typeof document.addEventListener === "undefined" || hidden === undefined) {
		console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
	} else {
		// Handle page visibility change   
		document.addEventListener(visibilityChange, handleVisibilityChange, false);
	}
});
