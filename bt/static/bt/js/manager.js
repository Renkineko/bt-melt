// polyfill prepend
// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/prepend()/prepend().md
(function (arr) {
	arr.forEach(function (item) {
	  if (item.hasOwnProperty('prepend')) {
		return;
	  }
	  Object.defineProperty(item, 'prepend', {
		configurable: true,
		enumerable: true,
		writable: true,
		value: function prepend() {
		  var argArr = Array.prototype.slice.call(arguments),
			docFrag = document.createDocumentFragment();
		  
		  argArr.forEach(function (argItem) {
			var isNode = argItem instanceof Node;
			docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
		  });
		  
		  this.insertBefore(docFrag, this.firstChild);
		}
	  });
	});
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]);

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/append()/append().md
(function (arr) {
	arr.forEach(function (item) {
	  if (item.hasOwnProperty('append')) {
		return;
	  }
	  Object.defineProperty(item, 'append', {
		configurable: true,
		enumerable: true,
		writable: true,
		value: function append() {
		  var argArr = Array.prototype.slice.call(arguments),
			docFrag = document.createDocumentFragment();
  
		  argArr.forEach(function (argItem) {
			var isNode = argItem instanceof Node;
			docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
		  });
  
		  this.appendChild(docFrag);
		}
	  });
	});
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]);

$(function() {
	var nb_theme = 0;
	var nb_song = 0;
	var final_results = [];
	
	// Initialisation websocket
	var websocket;
	
	function bt_start() {
		$('#btnStart').prop('disabled', true);
		$('#btnTheme, #btnNoTheme, #btnStop').prop('disabled', false);
		nb_theme = 0;
		nb_song = 0;
		final_results = [];
		compute_mvp();
	}
	
	function final_result_sort(a, b) {
		if (a.final_score > b.final_score) {
			return -1;
		}
		
		if (b.final_score > a.final_score) {
			return 1;
		}
		
		if (a.slugified_username > b.slugified_username) {
			return 1;
		}
		
		return b.slugified_username > a.slugified_username ? -1 : 0;
	}
	
	function bt_stop() {
		$('#btnStart').prop('disabled', false);
		$('#btnTheme, #btnNoTheme, #btnSong, #btnStop, #btnTimerTheme, #btnTimerSong').prop('disabled', true);
	}
	
	function compute_mvp() {
		final_results.sort(final_result_sort);
		
		var prev_score = null;
		var indice = 1;
		
		$('#mvp1, #mvp2, #mvp3, #mvp4').each(function() {
			$(this).find(' span:last').text('');
			$(this).find(' h6 span').text('NC');
		});
		
		final_results.forEach(function(user) {
			if (prev_score === null) {
				$('#mvp'+indice+' span:last').text(user.username);
				$('#mvp'+indice+' h6 span').text(user.final_score);
				prev_score = user.final_score;
			}
			else {
				if (prev_score === user.final_score) {
					$('#mvp'+indice+' span:last').text($('#mvp'+indice+' span:last').text() + ', ' + user.username);
				}
				else {
					indice++;
					
					if (indice < 4) {
						$('#mvp'+indice+' span:last').text(user.username);
						$('#mvp'+indice+' h6 span').text(user.final_score);
						prev_score = user.final_score;
					}
					else {
						return false;
					}
				}
			}
		});
		
		var next_score = null;
		var vrai_avant_dernier = false;
		for (var i=final_results.length; i--; ) {
			if (next_score === null) {
				next_score = final_results[i].final_score;
			}
			else {
				if (next_score === final_results[i].final_score && !vrai_avant_dernier) {
					// Le score est le même que celui du tour précédent, et on est pas sur le réel avant-dernier. On oublie.
					continue;
				}
				
				if (next_score >= prev_score || final_results[i].final_score >= prev_score) {
					// Le score de l'avant dernier est supérieur ou égal à celui du 3ème, on a pas d'avant-dernier à proprement parler...
					break;
				}
				
				if (!vrai_avant_dernier) {
					vrai_avant_dernier = true;
					$('#mvp4 span:last').text(final_results[i].username);
					$('#mvp4 h6 span').text(final_results[i].final_score);
					next_score = final_results[i].final_score;
					continue;
				}
				
				if (next_score !== final_results[i].final_score) {
					break;
				}
				
				if ($('#mvp4 span:last').text() !== '') {
					$('#mvp4 span:last').text($('#mvp4 span:last').text() + ', ' + final_results[i].username);
				}
			}
		}
	}
	
	function new_theme(to_guess, theme_hint, only_history) {
		$('#answers .collapse').collapse('hide');
		$('div.current-theme').removeClass('current-theme');
		
		nb_theme++;
		nb_song = 0;
		
		var expected_theme = 'Thème N°' + nb_theme;
		if (to_guess) {
			if (theme_hint !== '') {
				expected_theme = theme_hint;
			}
		}
		
		var tpl = to_guess ? '#tplTheme' : '#tplNoTheme';
		
		var node = document.importNode(document.querySelector(tpl).content, true);
		node.querySelector('.card-header a').setAttribute('href', '#collapse'+nb_theme);
		node.querySelector('.card-header a').setAttribute('aria-controls', 'collapse'+nb_theme);
		node.querySelector('.collapse').setAttribute('aria-labelledby', 'collapse'+nb_theme);
		node.querySelector('.collapse').setAttribute('id', 'collapse'+nb_theme);
		node.querySelector('h6 span').textContent = expected_theme;
		
		if (to_guess) {
			node.querySelector('td.expected').textContent = expected_theme;
		}
		
		document.querySelector('#answers').prepend(node);
		
		if (!only_history) {
			var command = to_guess ? 'theme' : 'no-theme';
			// Envoyer la requête à la websocket pour signifier le nouveau thème (pas à deviner)
			ws_send({
				"command": command,
				"number": nb_theme,
				"hint": expected_theme,
			});
		}
		
		$('#btnSong').prop('disabled', false);
		$('#btnTimerTheme').prop('disabled', !to_guess);
	}
	
	function new_song(song_hint, only_history) {
		$('tr.current-song').removeClass('current-song');
		
		nb_song++;
		
		var expected_song = song_hint;
		if (expected_song === '') {
			expected_song = 'Musique N°' + nb_song;
		}
		
		var node = document.importNode(document.querySelector('#tplSong').content, true);
		node.querySelector('td.expected').textContent = expected_song;
		
		document.querySelector('div.current-theme tbody.tbody-song').prepend(node);
		
		$('#expectedSongAnswer').modal('hide');
		
		if (!only_history) {
			// Envoyer la requête à la websocket pour signifier la nouvelle chanson
			ws_send({
				"command": "song",
				"number": nb_song,
				"hint": expected_song,
				"placeholder": $('#placeholderSong').val()
			});
		}
		
		$('#btnTimerSong').prop('disabled', false);
	}
	
	function receive_answer(target, answer, username, slugified_username, score) {
		var selector = '';
		var relid = '';
		var command = '';
		switch (target) {
			case 'theme':
				selector = 'theme .theme';
				relid = 't' + nb_theme;
				command = 'theme';
			break;
			
			case 'song':
				selector = 'song';
				relid = 't' + nb_theme + 's' + nb_song;
				command = 'song';
			break;
			
			default:
				return;
		}
		
		var node = $('#answers .current-' + selector + ' .answers .row .div-answer[nickname='+slugified_username+']');
		if (node.length > 0) {
			if (node.find('button.active').length === 0) {
				node.find('.user-answer').text(answer);
				node.find('.btn-group button').prop('disabled', false);
			}
		}
		else {
			node = document.importNode(document.querySelector('#tplAnswer').content, true);
			node.querySelector('.div-answer').setAttribute('nickname', slugified_username);
			node.querySelector('.nickname').textContent = username;
			node.querySelector('.user-answer').textContent = answer;
			node.querySelector('.btn-group').setAttribute('relid', relid);
			
			document.querySelector('#answers .current-' + selector + ' .answers .row').append(node);
		}
		
		$('#answers .current-' + selector + ' button.not-initialized').each(function() {
			$(this).off('click').on('click', function() {
				var final_result = final_results.find(function(user) {
					return user.slugified_username == slugified_username;
				});
				
				var score_variation = 0;
				
				if ($(this).is('.active')) {
					ws_send({
						"command": "cancel-note-" + command,
						"username": $(this).closest('.div-answer[nickname]').find('.nickname').text(),
						"relid": $(this).closest('.btn-group').attr('relid'),
					});
					score_variation-= parseInt($(this).text(), 10);
					$(this).removeClass('active');
					$(this).closest('.div-answer[nickname]').removeClass('bg-light');
				}
				else {
					ws_send({
						"command": "note-" + command,
						"score": parseInt($(this).text(), 10),
						"answer": $(this).closest('.div-answer[nickname]').find('.user-answer').text(),
						"username": $(this).closest('.div-answer[nickname]').find('.nickname').text(),
						"relid": $(this).closest('.btn-group').attr('relid'),
					});
					if ($(this).closest('.btn-group').find('button.active').length > 0) {
						score_variation-= parseInt($(this).closest('.btn-group').find('button.active').text(), 10);
					}
					score_variation+= parseInt($(this).text(), 10);
					$(this).closest('.btn-group').find('button').removeClass('active');
					$(this).addClass('active');
					$(this).closest('.div-answer[nickname]').addClass('bg-light');
				}
				
				if (final_result) {
					final_result.final_score+= score_variation;
				}
				else {
					final_results.push({
						username: username,
						slugified_username: slugified_username,
						final_score: score_variation,
					});
				}
				
				compute_mvp();
			});
			$(this).removeClass('not-initialized');
		});
		
		if (score) {
			$('#answers .current-' + selector + ' .answers .row .div-answer[nickname='+slugified_username+'] button[val=' + score + ']').addClass('active');
			$('#answers .current-' + selector + ' .answers .row .div-answer[nickname='+slugified_username+']').addClass('bg-light');
			var final_result = final_results.find(function(user) {
				return user.slugified_username == slugified_username;
			});
			
			if (final_result) {
				final_result.final_score+= score;
			}
			else {
				final_results.push({
					username: username,
					slugified_username: slugified_username,
					final_score: score,
				});
			}
			
			compute_mvp();
		}
	}
	
	function cancel_answer(target, slugified_username) {
		var selector = '';
		switch (target) {
			case 'theme':
				selector = 'theme .theme';
			break;
			
			case 'song':
				selector = 'song';
			break;
			
			default:
				return;
		}
		
		var node = $('#answers .current-' + selector + ' .answers .row .div-answer[nickname='+slugified_username+']');
		if (node.length > 0) {
			if (node.find('button.active').length === 0) {
				node.find('.user-answer').html('<i class="fas fa-ban"></i>');
				node.find('.btn-group button').prop('disabled', true);
			}
		}
	}
	
	function decompte(buttons, stopwatch_element, timer) {
		if (timer <= 0) {
			stopwatch_element.text('');
			buttons.prop('disabled', false);
		}
		else {
			timer--;
			stopwatch_element.text(' ('+timer + ')');
			setTimeout(function() {
				decompte(buttons, stopwatch_element, timer);
			}, 1000);
		}
	}
	
	function begin_theme_timer(seconds, only_history) {
		if (!only_history) {
			ws_send({
				"command": "begin-timer-theme",
			});
		}
		$('#btnTheme, #btnNoTheme').prop('disabled', true);
		$('#btnTimerTheme').prop('disabled', true);
		decompte($('#btnTheme, #btnNoTheme'), $('#remainingTimeTheme'), seconds);
		// setTimeout(function() {
		// 	$('#btnTheme, #btnNoTheme').prop('disabled', false);
		// }, seconds * 1000);
	}
	
	function begin_song_timer(seconds, only_history) {
		if (!only_history) {
			ws_send({
				"command": "begin-timer-song",
			});
		}
		$('#btnSong').prop('disabled', true);
		$('#btnTimerSong').prop('disabled', true);
		decompte($('#btnSong'), $('#remainingTimeSong'), seconds);
		// setTimeout(function() {
		// 	$('#btnSong').prop('disabled', false);
		// }, seconds * 1000);
	}
	
	function ws_init() {
		var protocole = location.protocol.split(':')[0] === 'https' ? 'wss' : 'ws';
		websocket = new WebSocket(protocole + '://'+location.host+'/wbsckt/bt/');
		
		websocket.onmessage = function(e) {
			var data = JSON.parse(e.data);
			
			console.log(data);
			
			if (data.error) {
				console.error(data.error);
				return;
			}
			
			switch(data['command']) {
				case 'recap':
					if (data['bt']) {
						if (data['bt'].active) {
							bt_start();
						}
						
						$('#answers').empty();
						data['bt'].themes.forEach(function(theme) {
							new_theme(theme.to_guess, theme.hint, true);
							
							theme.answers.forEach(function(answer) {
								receive_answer('theme', answer.answer, answer.username, answer.slugified_username, answer.score);
								if (answer.answer === '') {
									cancel_answer('theme', answer.slugified_username);
								}
							});
							
							if (theme.active) {
								if (theme.seconds_left) {
									begin_theme_timer(theme.seconds_left, true)
								}
								else {
									if (theme.locked) {
										$('#btnTimerTheme').prop('disabled', true);
									}
									else {
										$('#btnTimerTheme').prop('disabled', false);
									}
								}
							}
							
							theme.songs.forEach(function(song) {
								new_song(song.hint, true);
								
								song.answers.forEach(function(answer) {
									receive_answer('song', answer.answer, answer.username, answer.slugified_username, answer.score);
									if (answer.answer === '') {
										cancel_answer('song', answer.slugified_username);
									}
								});
								
								if (song.active) {
									if (song.seconds_left) {
										begin_song_timer(song.seconds_left, true)
									}
									else {
										if (song.locked) {
											$('#btnTimerSong').prop('disabled', true);
										}
										else {
											$('#btnTimerSong').prop('disabled', false);
										}
									}
								}
							});
						});
						
						if (!data['bt'].active) {
							bt_stop();
						}
					}
				break;
				
				case 'theme-answer':
					receive_answer('theme', data['answer'], data['username'], data['slugified_username']);
				break;
				
				case 'song-answer':
					receive_answer('song', data['answer'], data['username'], data['slugified_username']);
				break;
				
				case 'cancel-theme-answer':
					cancel_answer('theme', data['slugified_username']);
				break;
				
				case 'cancel-song-answer':
					cancel_answer('song', data['slugified_username']);
				break;
			}
		};
	}
	
	function ws_send(message) {
		switch (websocket.readyState) {
			case websocket.CONNECTING:
				return setTimeout(function() {
					ws_send(message)
				}, 200);
				
			case websocket.OPEN:
				return websocket.send(JSON.stringify(message));
				
			case websocket.CLOSED:
			case websocket.CLOSING:
				ws_init();
				return setTimeout(function() {
					ws_send(message)
				}, 200);
		}
	}
	
	ws_init();
	
	// Initialisation buttons
	$('#btnStop').off('click').on('click', function() {
		if (confirm("Veux-tu vraiment arrêter ce Blind-test ? Il ne sera pas récupérable : les scores seront supprimés de la mémoire, les participants ne pourront plus participer, bref fin du game quoi. Un récap sera affiché jusqu'à ce que tu quittes cette page ou que tu relances un BT.")) {
			// Envoyer la requête à la websocket pour signifier l'arrêt du BT
			ws_send({
				"command": "stop",
			});
			
			bt_stop();
		}
	});
	$('#btnStart').off('click').on('click', function() {
		// Envoyer la requête à la websocket pour signifier le début du BT
		ws_send({
			"command": "start",
		});
		
		bt_start();
	});
	$('#btnTheme').off('click').on('click', function() {
		$('#expectedTheme').val('');
		$('#expectedThemeAnswer').modal('show');
		
		console.log('TODO : click on ' + $(this).attr('id'));
	});
	$('#btnNoTheme').off('click').on('click', function() {
		new_theme(false, '');
	});
	$('#btnSong').off('click').on('click', function() {
		$('#expectedSong').val('');
		$('#expectedSongAnswer').modal('show');
		
		console.log('TODO : click on ' + $(this).attr('id'));
	});
	$('#btnTimerTheme').off('click').on('click', function() {
		begin_theme_timer(15);
	});
	$('#btnTimerSong').off('click').on('click', function() {
		begin_song_timer(15);
	});
	$('#btnAddTheme').off('click').on('click', function() {
		$('#expectedThemeAnswerForm').submit();
	});
	$('#btnAddSong').off('click').on('click', function() {
		$('#expectedSongAnswerForm').submit();
	});
	$('button[placeholder-value').on('click', function() {
		$('#placeholderSong').val($(this).attr('placeholder-value'));
	});
	
	$('#btnScores').off('click').on('click', function() {
		final_results.sort(final_result_sort);
		
		$('#finalResultsDiv').empty();
		final_results.forEach(function(score) {
			var node = document.importNode(document.querySelector('#tplResult').content, true);
			node.querySelector('span.finalResultUsername').textContent = score.username;
			node.querySelector('span.finalResultPoints').textContent = score.final_score !== null ? score.final_score : 'NC';
			document.querySelector('#finalResultsDiv').append(node);
		});
		
		$('#finalResults').modal('show');
	});
	
	// --------------------------- GESTION MODAL ---------------------------
	$('#expectedThemeAnswerForm').off('submit').on('submit', function() {
		try {
			new_theme(true, $('#expectedTheme').val());
			$('#expectedThemeAnswer').modal('hide');
		}
		catch(e) {
			console.error(e);
		}
		
		return false;
	});
	$('#expectedThemeAnswer').on('shown.bs.modal', function() {
		$('#expectedTheme').focus();
	});
	
	$('#expectedSongAnswerForm').off('submit').on('submit', function() {
		try {
			new_song($('#expectedSong').val());
		}
		catch(e) {
			console.error(e);
		}
		return false;
	});
	$('#expectedSongAnswer').on('shown.bs.modal', function() {
		$('#expectedSong').focus();
	});
});
