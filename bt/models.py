"""
Gestion des models pour le blind test
"""
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class BlindTest(models.Model):
    """
    Classe unique qui permet juste de savoir si le blind test est lancé ou pas.
    C'est possible de faire un singleton en django ?
    """
    active = models.BooleanField("actif", default=False)
    
class Theme(models.Model):
    """
    Thème à deviner (ou pas) dans le blind test
    """
    to_guess = models.BooleanField("à deviner ?", default=True)
    indice = models.PositiveSmallIntegerField("n°", default=1)
    hint = models.CharField("nom ou indice", max_length=255, blank=True)
    locked = models.BooleanField("verrouillé", default=False)
    active = models.BooleanField("actif", default=True)
    started_timer = models.DateTimeField("début timer", null=True)
    blindtest = models.ForeignKey(to=BlindTest, on_delete=models.CASCADE, related_name="themes")
    
    def get_relid(self):
        """
        Retourne l'identifiant relatif pour ce thème
        """
        return "t%d" % self.indice
    
class Song(models.Model):
    """
    Morceau à deviner dans le thème du blind test
    """
    indice = models.PositiveSmallIntegerField("n°", default=1)
    hint = models.CharField("nom ou indie", max_length=255, blank=True)
    locked = models.BooleanField("verrouillé", default=False)
    active = models.BooleanField("actif", default=True)
    started_timer = models.DateTimeField("début timer", null=True)
    placeholder = models.CharField("placeholder", max_length=255, blank=True)
    theme = models.ForeignKey(to=Theme, on_delete=models.CASCADE, related_name="songs")
    
    def get_relid(self):
        """
        Retourne l'identifiant relatif pour ce morceau
        """
        return "t%ds%d" % (self.theme.indice, self.indice)
    
    
class UserThemeAnswer(models.Model):
    """
    Gère les réponses des joueurs aux thèmes
    """
    
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    theme = models.ForeignKey(to=Theme, on_delete=models.CASCADE, related_name="themes_answers")
    answer = models.CharField("réponse", max_length=255, blank=True)
    score = models.PositiveSmallIntegerField("points", null=True)
    
    class Meta:
        unique_together = ('user', 'theme')
    
class UserSongAnswer(models.Model):
    """
    Gère les réponses des joueurs aux morceaux
    """
    
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    song = models.ForeignKey(to=Song, on_delete=models.CASCADE, related_name="songs_answers")
    answer = models.CharField("réponse", max_length=255, blank=True)
    score = models.PositiveSmallIntegerField("points", null=True)
    
    class Meta:
        unique_together = ('user', 'song')
    